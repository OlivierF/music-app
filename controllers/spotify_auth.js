var express = require('express');
var router = express.Router();
var request = require('request');
var querystring = require('querystring');

var client_id = 'fb68e21bae1a4824a30181b570a788d4'; // Replace with your Spotify application Client ID
var client_secret = '3cb33e31f29440648d372e6f667525a2'; // Replace with your Spotify application Client Secret
var redirect_uri = 'http://localhost:3001/spotify_auth/callback'; // Spotify API: Your redirect uri
var frontend_uri = 'http://localhost:3000'

/**
 * Sends back the Spotify authentication URL in a json
 */
router.get('/login', function(req, res) {
  console.log("router.get(\'/login\')");
  res.json(['https://accounts.spotify.com/authorize?' +
    querystring.stringify({
      response_type: 'code',
      client_id: client_id,
      scope: 'user-read-private user-read-email user-top-read',
      redirect_uri
    })]);
});

/**
 * After successful authentication with spotify,
 * redirects the user to the frontend with a valid access_token
 */
router.get('/callback', function(req, res) {
  let code = req.query.code || null
  let authOptions = {
    url: 'https://accounts.spotify.com/api/token',
    form: {
      code: code,
      redirect_uri,
      grant_type: 'authorization_code'
    },
    headers: {
      'Authorization': 'Basic ' + (new Buffer(
        client_id + ':' + client_secret
      ).toString('base64'))
    },
    json: true
  }
  request.post(authOptions, function(error, response, body) {
    var access_token = body.access_token
    let uri = frontend_uri || 'http://localhost:3000'
    res.redirect(uri + '?access_token=' + access_token)
  });
});

module.exports = router;
