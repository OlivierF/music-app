# Artist Suggestions based on your Spotify Account

This web app uses the Spotify API to collect data about a user (top artists on a given time range) and provides him with a list of similar artists for each "favorite artist.

[Demonstration video](https://youtu.be/jj40kvUaMHk)


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Installing

Installation of the modules for backend:

```
npm install
```

Installation of the modules for frontend:

```
cd client/
npm install
```

You should create an application at [https://developer.spotify.com/my-applications/](https://developer.spotify.com/my-applications/) and in ./controllers/spotify_auth.js replace client_id and client_secret with your own application client ID and client secret (the client secret provided here is not valid)


## Running locally

Launch the server:

```
npm run start

```

Then, launch the client:
```
cd client/
npm run start
```


## Tech stack

This project uses mainly:

On frontend:
  - [React](https://reactjs.org)
  - [reactstrap](http://reactstrap.github.io)

On backend:
  - [Express.js](https://expressjs.com)


The project structure was built using express-generator and create-react-app so there may be still useless files.
The files containing my code are in:
	- frontend:
    - ./client/src/
	- backend: 	
    - ./controllers/spotify_auth.js
    - ./bin/server.
    - .app.js
