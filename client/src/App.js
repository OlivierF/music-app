import React, { Component } from 'react';
import { Alert, Button, ButtonGroup, Col, Container, Media, Row } from 'reactstrap';
import queryString from 'query-string';
import ArtistCard from './Components/ArtistCard';
import { fetchUserData, fetchFavoriteArtists, fetchSimilarArtists } from './Actions/Fetching';

import './App.css';

/**
 *
 *
 */
class App extends Component {
  constructor() {
    super();
    this.state = {
      loginURI: [],
      accessToken: queryString.parse(window.location.search).access_token,
      timeRangeSelected: 'long_term',
    };
  }

  /**
   * Fetches the Spotify authentication URL if there is no accessToken saved in state.
   *
   * Else, fetches user data from the Spotify API and saves it in state.
   *       fetches user's favorite artists and saves them in state.
   */
  componentDidMount() {
    if (!this.state.accessToken) {
      fetch('/spotify_auth/login')
        .then(res => res.json())
        .then(uri => this.setState({ loginURI: uri }));
    } else {
      fetchUserData(this.state.accessToken)
        .then(data => this.setState({
          userData: {
            name: data.display_name ? data.display_name : data.id,
            image: data.images[0],
          },
        }));
      this.fetchAndSaveFavoriteArtists(this.state.accessToken, this.state.timeRangeSelected);
    }/* endif(accessToken) */
  } /* componentDidMount() */

  /**
   * Gets called when the user clicks on a "Similar Artists" button in an
   *  ArtistCard
   *
   * If the artist isn't already the one selected/no artist has been selected,
   * fetches its similar artist and saves them in state
   */
  onSimilarArtistButtonClick(artist) {
    window.console.log(`Show artists similar to ${artist.artistData.name}`);
    if (this.state.selectedArtist) {
      if (this.state.selectedArtist.id !== artist.artistData.id) {
        this.fetchAndSaveSimilarArtists(artist);
      }
    } else { this.fetchAndSaveSimilarArtists(artist); }
  }

  /**
     * Gets called when the user clicks on a "Time Range" button.
     *
     * If the timeRange isn't already the one selected, fetches and saves user's
     * favorite artists corresponding to the selected timeRange and changes state
     */
  onTimeRangeBtnClick(timeRange) {
    if (this.state.timeRangeSelected !== timeRange) {
      this.setState({ timeRangeSelected: timeRange });
      this.fetchAndSaveFavoriteArtists(this.state.accessToken, timeRange);
    }
  }

  /**
   * Gets called by onSimilarArtistButtonClick()
   *
   * Changes state.selectedArtist
   * Fetches artist similar artists and saves them in state
   */
  fetchAndSaveSimilarArtists(artist) {
    this.setState({ selectedArtist: { name: artist.artistData.name, id: artist.artistData.id } });
    fetchSimilarArtists(this.state.accessToken, artist.artistData.id)
      .then(data => this.setState({
        similarArtists: data,
      }));
  }

  /**
   * Gets called by this.componentDidMount() or by onTimeRangeBtnClick()
   *
   * Fetches users favorite artist in the timeRangeSelected and saves them in state
   */

  fetchAndSaveFavoriteArtists(accessToken, timeRangeSelected) {
    fetchFavoriteArtists(accessToken, timeRangeSelected)
      .then()
      .then(data => this.setState({
        favoriteArtistsData: data.items.map((artist) => {
          return ({
            artistData: artist,
            similarArtists: {},
          });
        }),
      }));
  }

  render() {
    window.console.log(this.state);
    return (
      <Container>
        <Row>
          <Col>
            <Alert color="info">This web app will allow you to discover artists similar to your favorite ones based on your Spotify account</Alert>
            {this.state.userData ?
              <Media>
                <Media left href="#">
                  {this.state.userData.image ? <img src={this.state.userData.image.url} alt="user" height="24" /> : undefined}
                </Media>
                <Media body>
                  <Col sm={{ offset: 0.1 }}>
                    You are logged in to Spotify as {this.state.userData.name}
                  </Col>
                </Media>
              </Media>
              :
              <a href={this.state.loginURI}>
                <Button color="success">Log in to Spotify</Button>
              </a>
            }
          </Col>
        </Row>
        {this.state.userData ?
          <Row>
            <Col>
              <h3>{this.state.userData.name}{"'s favorite artists are:"}</h3>
            </Col>
            <Col>
              <ButtonGroup>
                <Button color="primary" onClick={() => this.onTimeRangeBtnClick('long_term')} active={this.state.timeRangeSelected === 'long_term'}>All-time</Button>
                <Button color="primary" onClick={() => this.onTimeRangeBtnClick('medium_term')} active={this.state.timeRangeSelected === 'medium_term'}>6 months</Button>
                <Button color="primary" onClick={() => this.onTimeRangeBtnClick('short_term')} active={this.state.timeRangeSelected === 'short_term'}>4 weeks</Button>
              </ButtonGroup>
            </Col>
          </Row> : null
          }
        <Row>
          {this.state.favoriteArtistsData ?
            this.state.favoriteArtistsData.map((artist) => {
              return (
                <Col xs="12" sm="6" md="4" lg="3" style={{ paddingBottom: '15px', paddingTop: '15px' }} key={artist.artistData.id}>
                  <ArtistCard data={artist} >
                    <Button onClick={() => this.onSimilarArtistButtonClick(artist)}>
                      Similar Artists
                    </Button>
                  </ArtistCard>
                </Col>
                );
          })
            : null
          }
        </Row>
        {this.state.similarArtists ?
          <Row>
            <h3>Artists similar to {this.state.selectedArtist.name}:</h3>
            <Row>{ this.state.similarArtists.artists.map(artist => <Col key={artist.id} xs="12" sm="6" md="3" lg="2"><a href={artist.uri}><Button color="success" block style={{ margin: '5px 5px 5px 5px' }} >{artist.name}</Button></a></Col>) }</Row>
          </Row> : null
          }
      </Container>
    );
  }
}

export default App;
