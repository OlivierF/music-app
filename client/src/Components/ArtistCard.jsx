import React from 'react';
import { Badge, Button, Card, CardBody, CardImg, CardTitle, CardSubtitle } from 'reactstrap';
import '../Styles/ArtistCard.css';

/*
 * This component renders a card with the provided artist photo, name,
 * genre, button with and uri to spotify and a similar artist button if it is
 * passed as props.children
 *
 * props.data.artistData must be in the same format at an artist in the Spotify API
 *
 */
/* eslint-disable react/prop-types */
function ArtistCard(props) {
  // window.console.log(props.data);
  return (
    <Card inverse style={{ backgroundColor: '#333', borderColor: '#333', height: '100%' }}>
      <CardImg top style={{ maxWidth: '' }} src={props.data.artistData.images[1].url} />
      <CardBody>
        <CardTitle>{props.data.artistData.name}</CardTitle>
        <CardSubtitle style={{ color: '#AAA' }}><Badge color="secondary" pill>{props.data.artistData.genres[0]}</Badge></CardSubtitle>
        <a href={props.data.artistData.uri}>
          <Button color="success">Listen on Spotify</Button>
        </a>
        {props.children}
      </CardBody>
    </Card>
  );
}
/* eslint-enable */

export default ArtistCard;
