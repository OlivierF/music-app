/*
 * fetches user data using an accessToken
 */
export const fetchUserData = function fetchUserData(accessToken) {
  return (
    fetch('https://api.spotify.com/v1/me', {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }).then(res => res.json())
  );
};

/*
 * fetches user's favorite artists using an accessToken and a timeRange
 * ('long_term' || 'medium_term' || 'short_term')
 */
export const fetchFavoriteArtists = function fetchFavoriteArtists(accessToken, timeRange) {
  return (
    fetch(`https://api.spotify.com/v1/me/top/artists?limit=4&time_range=${timeRange}`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }).then(res => res.json())
  );
};

/*
 * fetches artists similar to an artist using its id and an accessToken,
 * both provided by the Spotify API
 */
export const fetchSimilarArtists = function fetchSimilarArtists(accessToken, artistID) {
  return (
    fetch(`https://api.spotify.com/v1/artists/${artistID}/related-artists`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }).then(res => res.json())
  );
};
